import React, { Component } from "react";
import "./App.css";
import { Container, Row, Col } from "reactstrap";
import Sidebar from "./components/Sidebar";
import Navbar2 from "./components/Navbar2";
import Route from "./components/Route";
import Map from "./components/Map";

class App extends Component {
  render() {
    return (
      <div>
        <Navbar2 />
        <Container fluid>
          <Row>
            <Col
              md="1"
              className="eq-column p-0"
              style={{
                backgroundColor: "#2b303b"
              }}
            >
              <Sidebar />
            </Col>
            <Col
              md="5"
              className="eq-column p-0 shadow-left"
              style={{
                backgroundColor: "#f1f1f1"
              }}
            >
              <Route />
            </Col>
            <Col md="6" className="eq-column p-0">
              <Map />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
