import React, { Component } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Form
} from "reactstrap";

class Navbar2 extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <Navbar dark color="bend" expand="md">
        <NavbarBrand href="/">
          <img
            src={process.env.PUBLIC_URL + "/images/bend-06.png"}
            height="28"
            alt=""
          />
        </NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Form className="form-inline my-2 my-lg-0">
            <Button color="success" className="my-2 my-sm-0 ml-3">
              <i className="fa fa-paper-plane" aria-hidden="true" /> {" "}
              NUEVO ENV&Iacute;O
            </Button>
          </Form>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink href="/components/">Emerson</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}

export default Navbar2;
