import React from "react";
import { Row, Col, Card, CardBody, FormGroup, Input } from "reactstrap";

const Waypoint = props => {
  return (
    <Card className={"mb-" + (props.last ? "3" : "0")}>
      <CardBody
        style={{
          padding: "0.3125rem 0.3125rem 0.3125rem 0.625rem"
        }}
      >
        <Row>
          <Col md="1" className="my-auto text-center">
            <h3>{props.letter}</h3>
          </Col>
          <Col md="11">
            <div className="form-group-compact">
              <FormGroup row className="multi first">
                <Col md="9">
                  <Input placeholder="Direcci&oacute;n" />
                </Col>
                <Col md="3">
                  <Input placeholder="Int/Of/Dpto" />
                </Col>
              </FormGroup>
              <FormGroup row className="middle">
                <Col>
                  <Input placeholder="Nombres y Apellidos del contacto" />
                </Col>
              </FormGroup>
              <FormGroup row className="multi middle">
                <Col md="7">
                  <Input placeholder="Correo electr&oacute;nico para notificaci&oacute;n" />
                </Col>
                <Col>
                  <Input placeholder="Tel&eacute;fono para notificaci&oacute;n" />
                </Col>
              </FormGroup>
              <FormGroup row className="last">
                <Col>
                  <Input placeholder="Instrucciones" />
                </Col>
              </FormGroup>
            </div>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default Waypoint;
