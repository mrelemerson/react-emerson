import React, { Component } from "react";
import { Container, Form, Row, Col, Button } from "reactstrap";
import Waypoint from "./Waypoint";
import Swap from "./Swap";
import Action from "./Action";
import PaymentMethod from "./payment-method";
import TermsConditions from "./terms-conditions";

const genCharArray = (charA, charZ) => {
  const a = [];
  let i = charA.charCodeAt(0),
    j = charZ.charCodeAt(0);
  for (; i <= j; ++i) {
    a.push(String.fromCharCode(i));
  }
  return a;
};

class Route extends Component {
  constructor(props) {
    super(props);
    this.state = {
      waypoints: genCharArray("A", "B")
    };
  }
  waypoints() {
    const size = this.state.waypoints.length;
    return this.state.waypoints.map((waypoint, index) => (
      <div key={index}>
        <Waypoint letter={waypoint} last={size === index + 1} />
        {size !== index + 1 ? <Swap /> : null}
      </div>
    ));
  }
  render() {
    return (
      <Form>
        <div
          className="oy-a pt-3 px-3"
          style={{
            height: "calc(100vh - 450px)"
          }}
        >
          {this.waypoints()}
        </div>
        <Action />
        <PaymentMethod />
        <Container
          fluid
          style={{
            borderTop: "1px solid #ced4da",
            padding: "10px 20px 0"
          }}
        >
          <Row>
            <Col sm="3">
              <h3 className="mb-0">PEN 7.00</h3>
              <Row>
                <Col>0 km</Col>
              </Row>
              <Row>
                <Col>0 min</Col>
              </Row>
            </Col>
            <Col sm="9">
              <Button color="primary" block={true}>
                Solicitar mensajero
              </Button>
              <Button color="link" block={true}>
                Reiniciar
              </Button>
            </Col>
          </Row>
        </Container>
      </Form>
    );
  }
}

export default Route;
