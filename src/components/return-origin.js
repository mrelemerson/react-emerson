import React from "react";

const ReturnOrigin = props => {
  return (
    <div className="clearfix return-origin my-3">
      <label className="switch float-left">
        <input type="checkbox" />
        <span className="slider round" />
      </label>
      <span className="float-left">RETORNAR AL ORIGEN</span>
    </div>
  );
};

export default ReturnOrigin;
