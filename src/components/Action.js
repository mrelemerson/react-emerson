import React from "react";
import {
  Container,
  Row,
  FormGroup,
  Col,
  Label,
  Input,
  Button
} from "reactstrap";

const Action = props => {
  return (
    <Container
      fluid
      className="shadow-top"
      style={{
        borderTop: "1px solid #ced4da",
        padding: "0 20px 0"
      }}
    >
      <Row>
        <Col sm="3" className="my-auto">
          <Button color="secondary">
            <i class="fa fa-plus-circle" aria-hidden="true" /> Adicionar punto
          </Button>
        </Col>
        <Col>
          <div className="clearfix switch-container py-2">
            <label className="switch float-left mb-0">
              <input type="checkbox" />
              <span className="slider round" />
            </label>
            <span className="float-left">RETORNAR AL ORIGEN</span>
          </div>
        </Col>
        <Col>
          <div className="clearfix switch-container py-2">
            <label className="switch float-left mb-0">
              <input type="checkbox" />
              <span className="slider round" />
            </label>
            <span className="float-left">OPTIMIZAR RUTA</span>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Action;
