import React from "react";
import {
  Container,
  Row,
  FormGroup,
  Col,
  Label,
  Input,
  Button
} from "reactstrap";

const PaymentMethod = props => {
  return (
    <Container
      fluid
      style={{
        borderTop: "1px solid #ced4da",
        padding: "10px 20px 0"
      }}
    >
      <Row>
        <Col className="payment-method">
          <FormGroup tag="fieldset">
            <legend>Forma de pago</legend>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="radio1" />
                Wallet <strong>(PEN 100)</strong>
              </Label>
              <Button color="link">Recargar</Button>
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="radio1" />
                <span>Tarjeta</span>
              </Label>
              <Input
                type="select"
                className="custom-select ml-3"
                style={{
                  width: "25%"
                }}
              >
                <option>Seleccione</option>
              </Input>
              <Button color="link">Agregar tarjeta</Button>
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="radio1" />
                <span> Linea de cr&eacute;dito</span>
              </Label>
            </FormGroup>
          </FormGroup>
        </Col>
      </Row>
    </Container>
  );
};

export default PaymentMethod;
