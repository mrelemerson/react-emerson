import React from "react";
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";

const Mapbox = ReactMapboxGl({
  accessToken:
    "pk.eyJ1IjoiZ2lhbmNvcnpvIiwiYSI6ImNqYmNyN2RjMDFxNXgyd3FnaW9td3Eyem0ifQ.fWpsPuZd255hbqQ1Qu_4yw"
});

const Map = props => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%"
      }}
    >
      <Mapbox
        style="mapbox://styles/mapbox/light-v9"
        containerStyle={{
          height: "100%",
          width: "100%"
        }}
        center={[-77.042793, -12.046374]}
        zoom={[13]}
      >
        <Layer type="symbol" id="marker" layout={{ "icon-image": "marker-15" }}>
          <Feature coordinates={[-77.042793, -12.046374]} />
        </Layer>
      </Mapbox>
    </div>
  );
};

export default Map;
