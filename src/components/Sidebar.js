import React from "react";
import { Nav, NavItem, NavLink } from "reactstrap";

const Sidebar = props => {
  return (
    <div className="sidebar pt-3">
      <Nav vertical className="flex-column" pills={true}>
        <NavItem>
          <NavLink href="#" active>
            <i className="fa fa-motorcycle" aria-hidden="true" />&nbsp;&nbsp;Hacer
            pedido
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="#">
            <i className="fa fa-inbox" aria-hidden="true" />&nbsp;&nbsp;Mis
            pedidos
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="#">
            <i className="fa fa-briefcase" aria-hidden="true" />&nbsp;&nbsp;Pagos
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink href="#">
            <i className="fa fa-star-o" aria-hidden="true" />&nbsp;&nbsp;Favoritos
          </NavLink>
        </NavItem>
      </Nav>
    </div>
  );
};

export default Sidebar;
