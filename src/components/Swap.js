import React from "react";

const Swap = props => {
  return (
    <div className="icon-container">
      <a href="javascript:void(0)">
        <i className="icon icon-swap" />
        Invertir puntos
      </a>
      <div className="vertical-line" />
    </div>
  );
};

export default Swap;
