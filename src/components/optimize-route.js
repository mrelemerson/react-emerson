import React from "react";

const OptimizeRoute = props => {
  return (
    <div className="clearfix optimize-route my-3">
      <label className="switch float-left">
        <input type="checkbox" />
        <span className="slider round" />
      </label>
      <span className="float-left">OPTIMIZAR RUTA</span>
    </div>
  );
};

export default OptimizeRoute;
