import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

const TermsConditions = props => {
  return (
    <div className="clearfix terms-conditions my-3">
      <label className="switch float-left">
        <input type="checkbox" />
        <span className="slider round" />
      </label>
      <span className="float-left">ACEPTO LOS <a href="">T&Eacute;RMINOS Y CONDICIONES</a></span>
    </div>
  );
};

export default TermsConditions;
